package _untouchable_.thingy; 


/**
 * Die Klasse Item beschreibt ein Item.
 * Ein Item hat die folgenden Eigenschaften:
 * <ul>
 *     <li>Farbe (color) - hierauf kann mit getColor() zugegriffen werden.</li>
 *     <li>Gewicht (weight) - hierauf kann mit getWeight() zugegriffen werden.</li>
 *     <li>Groesse (size) - hierauf kann mit getSize() zugegriffen werden.</li>
 *     <li>Wert (value) - hierauf kann mit getValue() zugegriffen werden.</li>
 * </ul>
 * Achtung!
 * <br />
 * Diese Klasse bzw. ihr Konstruktor akzeptiert null als zulaessigen Wert fuer jedes seiner Felder.
 *<br />
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  171217
 */
final public class Item {
    
    // Die Farbe eines Items
    final private Color color;
    
    // Das Gewicht eines Items
    final private Weight weight;
    
    // Die Groesse eines Items
    final private Size size;
    
    // Der Wert eines Items
    final private Long value;
    
    
    
    
    
    /**
     * Der Konstruktor erzeugt ein Item.<br />
     * Achtung!<br />
     * Der Konstruktor akzeptiert null als zulaessigen Wert fuer jedes seiner Felder.<br />
     *<br />
     *
     * @param color  bestimmt die Farbe des zu erzeugenden Items.
     * @param size   bestimmt die Groesse des zu erzeugenden Items.
     * @param weight bestimmt das Gewicht des zu erzeugenden Items.
     * @param value  bestimmt den Wert des zu erzeugenden Items.
     */
    public Item(
        final Color  color,
        final Size   size,
        final Weight weight,
        final Long   value
    ){
        this.color  = color;
        this.size   = size;
        this.weight = weight;
        this.value  = value;
    }//constructor()
    
    
    
    
    
    @Override
    public boolean equals( final Object otherObject ){
        if( this == otherObject )  return true;
        if( null == otherObject )  return false;
        if( getClass()!=otherObject.getClass() )  return false;
        
        final Item other = (Item)( otherObject );
        if( size != other.size  )  return false;
        if( color != other.color  )  return false;
        if( weight != other.weight)  return false;
        if( isUnequal( value, other.value ))  return false;         // NOTE: constants only exist once -> hence "!=" would be fine, too ;-)
        return true;
    }//method()
    //
    private static boolean isUnequal( final Object o1,  final Object o2 ){
        return (o1!=o2) && ( (null==o1) || ( ! o1.equals( o2 )));
    }//method()
    
    @Override
    public int hashCode(){
        final int prime = 31;
        int hashCode =              ((null==size) ? 0 : size.hashCode());
        hashCode = prime*hashCode + ((null==color)  ? 0 : color.hashCode());
        hashCode = prime*hashCode + ((null==weight) ? 0 : weight.hashCode());
        hashCode = prime*hashCode + ((null==value)  ? 0 : value.hashCode());
        return  hashCode;
    }//method();
    
    @Override
    public String toString(){
        return String.format(
            "[<%s>: %s %s %s %s]",
            Item.class.getSimpleName(),
            color,
            size,
            weight,
            value
        );
    }//method()
    
    
    /**
     * getColor() liefert die Farbe des jeweiligen Items.
     */
    public Color getColor(){ return color; }
    
    /**
     * getSize() liefert die Groesse des jeweiligen Items.
     */
    public Size getSize(){ return size; }
    
    /**
     * getSpeed() liefert die Gewicht des jeweiligen Items.
     */
    public Weight getWeight(){ return weight; }
    
    /**
     * getSize() liefert den Wert des jeweiligen Items.
     */
    public Long getValue(){ return value; }
    
}//class
