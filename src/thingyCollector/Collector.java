package thingyCollector;

import java.util.Collection;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import _untouchable_.thingy.Item;

public class Collector implements Collector_I {

    // Attributes
    List<Set<Item>> listOfSets;

    // Constructor
    public Collector() {
        listOfSets = new ArrayList<Set<Item>>();
    }

    // Methods
    /**
     * @param item The item that will be processed.
     * @return Collection of requestedNumberOfDifferentItems unique items that can be build with the processed one or if not null.
     */
    @Override
    public Collection<Item> process(Item item) {
        boolean isItemSet = false;
        int listOfSetsPosition = 0;

        while (!isItemSet) {
            // if Set at position doesn't exist, create one
            if (listOfSets.size() == listOfSetsPosition) {
                addSetToList();
                final Set<Item> currSet = listOfSets.get(listOfSetsPosition);
                currSet.add(item);
                return null;
            } else {          
                final Set<Item> currSet = listOfSets.get(listOfSetsPosition);
                if (currSet.add(item)) {
                    isItemSet = true;
                    // return set if size is atleast requestedNumberOfDifferentItems
                    if (currSet.size() >= requestedNumberOfDifferentItems) {
                        Collection<Item> result = currSet;
                        listOfSets.remove(listOfSetsPosition);
                        return result;
                    } else {
                        return null;
                    }
                } else {
                    // if duplicate exists check next position
                    listOfSetsPosition++;
                }
            }
        }
        return null;
    }

    /**
     * Resets the Collector.
     */
    @Override
    public void reset() {
        listOfSets.clear();
    }

    private void addSetToList() {
        Set<Item> setOfItems = new HashSet<Item>();
        listOfSets.add(setOfItems);
    }
}
